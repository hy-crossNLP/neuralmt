{
 "metadata": {
  "name": "",
  "signature": "sha256:9b536cc5a677952b9ea1e445c5071e6c559c1d67eb49123208d3fd7a6a0fa6c3"
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Keras4NLP\n",
      "\n",
      "This is a small tutorial on using [Keras](https://keras.io/), a high-level neural network library, for NLP tasks. In particular we will look at the [SIGMORPHON 2016 shared task](http://ryancotterell.github.io/sigmorphon2016/) on morphological reinflection, where all of the neural network-based approaches beat all other systems by a large margin.\n",
      "\n",
      "My own system was developed over a weekend, which was an important motivation for using Keras. High-level libraries such as this generally make life easier when you want to do something relatively standard, but can become unbearably annoying if you want to do anything not supported out-of-the-box.\n",
      "\n",
      "## Theano and Keras on CSC\n",
      "\n",
      "While simple networks with smallish datasets are possible to train with a CPU only, for real work you will need a GPU. I use the [GPU cluster](https://research.csc.fi/taito-gpu) on CSC.\n",
      "\n",
      "Make sure to get an account on CSC, and log in to `taito-gpu.csc.fi`. Install the bleeding-edge version of Theano following the [instructions on their webpage](http://deeplearning.net/software/theano_versions/dev/install_ubuntu.html), then clone the Keras repository and install. You may need to install some dependencies as well.\n",
      "\n",
      "In the CSC environment, I use the modules `cuda/7.5` and `python-env/3.4.1`. Load these in your `.bash_profile` as well as in your launch scripts, to make sure there are no incompatibilities when you launch jobs on the GPU cluster.\n",
      "\n",
      "Jobs are launched using SLURM scripts. This is an example of how to launch `program.py` on the `gputest` part of the cluster (normally you will want to use `gpu` or `gpulong` instead, see the CSC webpage for details). The job has at most 15 minutes of runtime (the maximum with `gputest`), if it is not finished by then it is terminated by force. It also has at most 4GB (4096MB) of RAM available, that's main memory, not GPU memory. Make sure this is sufficient, too. There is also an email address you should fill in, so you get notifications when a job is finished.\n",
      "\n",
      "```\n",
      "#!/bin/bash -l\n",
      "\n",
      "#SBATCH -J example\n",
      "#SBATCH -o stdout.log.%j\n",
      "#SBATCH -e stderr.log.%j\n",
      "#SBATCH -t 00:15:00\n",
      "#SBATCH -N 1\n",
      "#SBATCH -p gputest\n",
      "#SBATCH --mem=4096\n",
      "#SBATCH --gres=gpu:1\n",
      "#SBATCH --mail-type=END\n",
      "#SBATCH --mail-user=your.email@example.com\n",
      "#SBATCH\n",
      "\n",
      "module purge\n",
      "module load python-env/3.4.1\n",
      "module load cuda/7.5\n",
      "module list\n",
      "\n",
      "cd ${SLURM_SUBMIT_DIR:-.}\n",
      "pwd\n",
      "\n",
      "echo \"Starting at `date`\"\n",
      "THEANO_FLAGS=optimizer=fast_run,device=gpu,floatX=float32 python3 program.py\n",
      "echo \"Finishing at `date`\"\n",
      "```\n",
      "\n",
      "If this file is called `launch-program.sh`, then you launch the batch job with `sbatch launch-program.sh`. After doing this, you should see something like\n",
      "\n",
      "`Submitted batch job 12345678`\n",
      "\n",
      "Output from the program will go into `stdout.log.12345678` and `stderr.log.12345678`, but this is mostly for debugging output. Since the file system is shared among the CSC systems, just write your output to a file.\n",
      "\n",
      "## Getting started\n",
      "\n",
      "By this point I assume that you have a working setup of Keras, so let's start with a simple example: a sequence-to-sequence network. This is an extremely flexible model, and the same network can be used for everything from machine translation (input: text in language A, output: same text in language B), morphological reinflection (input: inflection A of lemma L, output: inflection B of the same lemma L), poetry generation (input: one line of a poem, output: the next line of a poem), question answering (input: question, output: answer).\n",
      "\n",
      "We begin by importing the necessary classes:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy as np\n",
      "import random\n",
      "from keras.models import Model\n",
      "from keras.layers import Input, LSTM, Embedding, Dense, RepeatVector, TimeDistributed, merge"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stderr",
       "text": [
        "Using Theano backend.\n"
       ]
      }
     ],
     "prompt_number": 1
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "If the above worked, that's a good sign.\n",
      "\n",
      "## Training data and encoding\n",
      "\n",
      "Next, we define some toy data to play around with:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# A list of tuples with (participle, past) verb forms\n",
      "participles, pasts = list(zip(*[tuple(s.split()) for s in [\n",
      "    'running ran',\n",
      "    'eating ate',\n",
      "    'talking talked',\n",
      "    'saying said',\n",
      "    'hiking hiked',\n",
      "    'speaking spoke',\n",
      "    'leaking leaked',\n",
      "    'walking walked',\n",
      "    'coding coded',\n",
      "    'backpropagating backpropagated'\n",
      "]]))\n",
      "\n",
      "# Define the symbol alphabet, including the padding symbol '#'\n",
      "letters = '#'  +''.join(sorted({c for s in participles+pasts for c in s}))\n",
      "# A map from letters to indexes\n",
      "letter_index = {c:i for i,c in enumerate(letters)}\n",
      "# The size of our alphabet\n",
      "n_letters = len(letters)\n",
      "# The maximum string length\n",
      "max_length = max(map(len, participles+pasts))\n",
      "\n",
      "n_letters, max_length, letters"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "pyout",
       "prompt_number": 2,
       "text": [
        "(20, 15, '#abcdeghiklnoprstuwy')"
       ]
      }
     ],
     "prompt_number": 2
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "From now on, we will need to work with numerical types rather than unicode strings. We define functions to convert between lists of integers and strings, and convert our dataset:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def encode_string(s): return [letter_index[c] for c in s]\n",
      "def decode_string(a): return ''.join(letters[i] for i in a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 3
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "For efficiency reasons all computations will be done using dense matrices. Since we deal with strings of different length we need to pad them into dense rectangular matrices. While this results in some unneccesary computation, it is well worth the increased speed of dense matrix operations (especially on a GPU). Keras has the helper function `pad_sequences` for this purpose."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "from keras.preprocessing.sequence import pad_sequences\n",
      "\n",
      "def encode_and_pad(words):\n",
      "    return pad_sequences(list(map(encode_string, words)), padding='post', maxlen=max_length)\n",
      "\n",
      "# Our inputs (x) will be the participles\n",
      "padded_x = encode_and_pad(participles)\n",
      "# And the outputs (y) will be the past tense forms\n",
      "padded_y = encode_and_pad(pasts)\n",
      "\n",
      "# As in the previous output, but with the padding.\n",
      "padded_x[0], padded_y[0], decode_string(padded_x[0]), decode_string(padded_y[0])"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "pyout",
       "prompt_number": 4,
       "text": [
        "(array([14, 17, 11, 11,  8, 11,  6,  0,  0,  0,  0,  0,  0,  0,  0], dtype=int32),\n",
        " array([14,  1, 11,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], dtype=int32),\n",
        " 'running########',\n",
        " 'ran############')"
       ]
      }
     ],
     "prompt_number": 4
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Note that the padded values are 0, **this is significant**, and the reason why we put the `#` symbol first (i.e. at position 0) in the `letters` list.\n",
      "\n",
      "Keras treats zero element in (embedded) sequences in a special way, essentially ignoring their values and making sure that the corresponding positions in the array do not contribute to the objective function we are optimzing. In other words, the resulting network is invariant to padding as long as we use zeros, which of course is what we want it to be.\n",
      "\n",
      "## Constructing the model\n",
      "\n",
      "Now to the fun part, defining the network architecture."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# Keras requires you to specify the maximum length of input sequences, we use the number of columns from our padded_x matrix.\n",
      "word = Input((max_length,))\n",
      "# Letters are represented by 32-dimensional embeddings covering the alphabet (of n_letters).\n",
      "embedded_word = Embedding(32, n_letters, mask_zero=True)(word)\n",
      "# The LSTM encoder will produce a 128-dimensional vector as output.\n",
      "encoding = LSTM(128)(embedded_word)\n",
      "# Repeat the encoding over the length of the output string.\n",
      "repeated_encoding = RepeatVector(max_length)(encoding)\n",
      "# Concatenate the repeated encoding with the embedded input string (in the last dimension).\n",
      "merged_encoding = merge([repeated_encoding, embedded_word], mode='concat')\n",
      "# The LSTM decoder will produce a _sequence_ of 128-dimensional vectors.\n",
      "decoded = LSTM(128, return_sequences=True)(merged_encoding)\n",
      "# Each of these vectors will be passed through (the same) fully connected layer with softmax activations.\n",
      "# The result is a sequence of distributions over the alphabet.\n",
      "output_word = TimeDistributed(Dense(n_letters, activation='softmax'))(decoded)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 5
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "That's it! Next step is to compile this network and train it.\n",
      "\n",
      "## Training the model"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "model = Model(input=word, output=output_word)\n",
      "# Here we specify the optimization algorithm ('adam' is a good default choice) and the loss function.\n",
      "# Our goal is to make the output distributions as close as possible to the values in the training set,\n",
      "# so we use categorical cross-entropy loss.\n",
      "model.compile(optimizer='adam', loss='categorical_crossentropy')"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 6
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "There is one complication here. Our target values, `padded_y`, contains one integer per letter, but what we actually need is a distribution over the alphabet for each letter (so that it is comparable to the output of the network). This requires one last conversion before we can start training:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "padded_y_dist = np.zeros(padded_y.shape+(n_letters,), dtype=np.float32)\n",
      "for i,row in enumerate(padded_y):\n",
      "    for j,letter in enumerate(row):\n",
      "        padded_y_dist[i,j,letter] = True\n",
      "\n",
      "# Note that all elements are zero except the one at the position of 'r' in our alphabet (this is the first letter of 'running')\n",
      "padded_y_dist[0,0], padded_y_dist[0,0,letter_index['r']]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "pyout",
       "prompt_number": 7,
       "text": [
        "(array([ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,\n",
        "         0.,  1.,  0.,  0.,  0.,  0.,  0.], dtype=float32), 1.0)"
       ]
      }
     ],
     "prompt_number": 7
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "from time import time\n",
      "t0 = time()\n",
      "history = model.fit(padded_x, padded_y_dist, nb_epoch=200, verbose=0)\n",
      "'%.2f seconds' % (time()- t0)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "pyout",
       "prompt_number": 8,
       "text": [
        "'26.84 seconds'"
       ]
      }
     ],
     "prompt_number": 8
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Training takes time, in particular the first time before Theano has compiled your functions. With optimizations turned on (as they should be when you run batch jobs on the GPU) it could easily take 10-15 minutes for your programs to start. Be patient!"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "def predict_and_decode(x):\n",
      "    predicted_y = model.predict(x)\n",
      "    # We choose the most probable symbol at each position.\n",
      "    return [decode_string(row.argmax(axis=-1)) for row in predicted_y]\n",
      "\n",
      "# Do predictions on the training set.\n",
      "predict_and_decode(padded_x)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "pyout",
       "prompt_number": 9,
       "text": [
        "['ran############',\n",
        " 'ate############',\n",
        " 'talked#########',\n",
        " 'said###########',\n",
        " 'hiked##########',\n",
        " 'spoke##########',\n",
        " 'leaked#########',\n",
        " 'walked#########',\n",
        " 'coded##########',\n",
        " 'backpropagated#']"
       ]
      }
     ],
     "prompt_number": 9
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Great success! Or, no, not really. We cheated by evaluating on training data, and this model is severely overfitted."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "predict_and_decode(encode_and_pad(['twirling', 'laughing', 'being']))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "pyout",
       "prompt_number": 10,
       "text": [
        "['sapkke#########', 'waalkkedddddddd', 'aid############']"
       ]
      }
     ],
     "prompt_number": 10
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The fundamental problem here is data sparsity. The SIGMORPHON shared task data contains about 12,000 pairs per language, better than the 10 pairs (several of them irregular!) in this example. Unfortunately that is a bit too much for an interactive demonstration on a CPU, so I will leave further experimentation to you.\n",
      "\n",
      "## The actual system\n",
      "\n",
      "**Beware of ugly code**\n",
      "\n",
      "My SIGMORPHON systems can be found [at Github](https://github.com/robertostling/sigmorphon2016-system). By now you should be able to decode most of `MorphonModel.build` in `sigmorphon-submission.py` (lines 46--138). There are a few key differences to the example given here:\n",
      "1. feedback is used in the decoder, so at time $t$ it has access to the output prediction of time $t-1$. This is unfortunately rather difficult to implement in Keras, and doing beam search becomes a nightmare, so I do not use it in this example.\n",
      "2. there are 1-dimensional convolutional layers before the encoder. The intuition behind this is that the LSTM encoder has access to information beyond the character level. In theory an LSTM is capable of encoding this information on its own, but it seems that in practice it is easier to train a good model with convolutional layers.\n",
      "3. dropout is used throughout, which is important to reduce overfitting. Note that [variational dropout](http://arxiv.org/abs/1512.05287) has become popular for recurrent networks, this is also implemented in Keras now.\n",
      "4. batch normalization is used throughout, which helps to speed up training. Recently, [layer normalization](http://arxiv.org/abs/1607.06450v1) seems like a better alternative for recurrent networks, this does not seem to be in Keras at the moment.\n",
      "5. since the SIGMORPHON task is to predict a specified word form (which in this example always was the past tense), the morphological description of the expected target word is given as a binary vector along with the input string.\n",
      "\n",
      "## Conclusions\n",
      "\n",
      "Keras is useful for rapid prototyping when your problem conforms to its constraints. In my case, three limitations became apparent:\n",
      "* decoders with feedback (where the output symbol at $t$ is used to predict the output at $t+1$) are difficult to support, I had to resort to a hack where a shifted version of the output is fed back into the input sequence. This is no problem during training, but at prediction time one has to generate a series of succesively longer sequences, which is very slow.\n",
      "* there is no support for beam search, in combination with the feedback issue above this meant decoding was so slow that prediction took up to a day for a few hundred word forms in the SIGMORPHON test set.\n",
      "* I can not find any way to share parameters between different types of layers, such as when you want to use the same character embeddings for both an encoder (an Embedding layer in Keras) and a decoder (where a Dense layer is used with the transposed embedding matrix).\n",
      "\n",
      "I still use Keras for simple problems, but for my own research I wrote my library, BNAS, which is [available at Github](https://github.com/robertostling/bnas). The philosophy of BNAS is closer to that of [Blocks](https://github.com/mila-udem/blocks) (which, despite my best efforts, I was unable to make a working implementation of the SIGMORPHON system in) or [Lasagne](https://github.com/Lasagne/Lasagne), both of which are trying to make it easier to use Theano rather than hiding it completely like Keras does. Anyone is welcome to try out BNAS, the examples include a [language model](https://github.com/robertostling/bnas/blob/master/examples/lm.py) and a [machine translation system](https://github.com/robertostling/bnas/blob/master/examples/nmt.py)."
     ]
    }
   ],
   "metadata": {}
  }
 ]
}